import argparse
import csv
import os
import sys
import time
from inspect import Traceback, currentframe, getframeinfo
from typing import TextIO

from dotenv import dotenv_values
from github import PaginatedList, Repository
from selenium import webdriver
from selenium.common.exceptions import WebDriverException, NoSuchElementException
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

import check_branches
import constants

no_changes_pattern: str = ' : No Changes.'
already_checked_it: str = ' : Already Checked It.'
already_timeout: str = ' : Already Timeout.'
interested_commits_only: str = ' : Contains Already Interested Commits only'
not_interested_commits_only: str = ' : Contains Not Interested Commits only'
not_interested_and_interested_commits_only: str = ' : Contains Not Interested & Already Interested Commits only'
page_loading_time_out_error: str = ' : Error - Timeout of page loading wait time'


# TODO: Move to common utils - get input from command line (via. arguments (another fn) / interactive (another fn))
def get_desired_git_hub_repository() -> str:
    """
    Identify the command line argument passed GitHub repository or
    Prompts the user for a GitHub repository (organization/repository) and returns it.

    Returns:
        str: The desired GitHub repository specified by the user.
    """
    command_line_arguments_parser = argparse.ArgumentParser(prog='GitHub-Repository-Forks-Analyzer',
                                                            description='Analyses forks of a GitHub repository',
                                                            allow_abbrev=False)
    command_line_arguments_parser.add_argument('--desiredGitHubRepository',
                                               help='The GitHub repository to analyze',
                                               metavar='desiredGitHubRepository',
                                               dest='desired_git_hub_repository')
    command_line_arguments = command_line_arguments_parser.parse_args()
    if command_line_arguments.desired_git_hub_repository:
        desired_git_hub_repository = command_line_arguments.desired_git_hub_repository
    else:
        desired_git_hub_repository = input(
            "Enter the desired github repository with organization name (organization/repository): ")
    return desired_git_hub_repository


def get_chrome_driver() -> WebDriver:
    options = webdriver.ChromeOptions()
    options.add_argument("--remote-debugging-port=9922")
    options.add_experimental_option('excludeSwitches', ['enable-logging'])
    # print('User : '+os.getlogin())
    # options.add_argument(
    #     "--user-data-dir=/home/"+os.getlogin()+"/.config/google-chrome2")
    # options.add_argument('--profile-directory=Default')
    # TODO: Check environment variable for chrome driver path
    return webdriver.Chrome(options=options)


def initialize() -> tuple[webdriver.Chrome, str]:
    return get_chrome_driver(), get_desired_git_hub_repository()


def load_first_page(desired_git_hub_website_url: constants.GithubUrls.GithubUrlEnums) -> tuple[str, webdriver.Chrome]:
    desired_git_hub_repository: str
    driver: WebDriver
    desired_git_hub_repository, driver = initialize()
    driver.get(constants.get_full_git_hub_url(desired_git_hub_repository=desired_git_hub_repository,
                                              git_hub_website_url=str(desired_git_hub_website_url.value)))
    return desired_git_hub_repository, driver


def is_item_included_in_csv(csv_file_with_path: str, data: str) -> bool:
    if os.path.exists(path=csv_file_with_path):

        csv_file: csv.reader = csv.reader(
            open(file=csv_file_with_path, mode="r"))

        row: list[str]
        for row in csv_file:

            if data == row[0]:
                return True

    return False


def is_item_not_included_in_csv(csv_file_with_path: str, data: str) -> bool:
    return not is_item_included_in_csv(csv_file_with_path=csv_file_with_path, data=data)


def is_list_already_included_in_csv(csv_file_with_path: str, list_to_check: list[str]) -> bool:
    item: str
    for item in list_to_check:
        if is_item_not_included_in_csv(csv_file_with_path=csv_file_with_path, data=item):
            return False
    return True


def is_list_already_included_among_two_csv_files(csv1_file_with_path: str, csv2_file_with_path: str,
                                                 list_to_check: list) -> bool:
    item: str
    for item in list_to_check:
        if is_item_not_included_in_csv(csv_file_with_path=csv1_file_with_path,
                                       data=item) and is_item_not_included_in_csv(
            csv_file_with_path=csv2_file_with_path, data=item):
            return False
    return True


def inner_join_list_with_csv(csv_file_with_path: str, list_to_check: list[str]) -> list[str]:
    result: list[str] = []
    item: str
    for item in list_to_check:
        if is_item_included_in_csv(csv_file_with_path=csv_file_with_path, data=item):
            result.append(item)
    return result


def append_single_column_row_to_csv(csv_file_with_path: str, data: str, is_interactive=False) -> None:
    append_row_to_csv(csv_file_with_path=csv_file_with_path,
                      column_list=[data], is_interactive=is_interactive)


def append_row_to_csv(csv_file_with_path: str, column_list: list, is_interactive=False) -> None:
    file: TextIO
    with open(file=csv_file_with_path, mode='a', newline='') as file:
        writer: csv.writer = csv.writer(file)
        writer.writerow(column_list)
    if is_interactive:
        input("Press Enter to continue...")


def append_list_to_csv(csv_file_with_path: str, list_to_append: list, is_interactive=False) -> None:
    item: str
    for item in list_to_append:
        append_single_column_row_to_csv(
            csv_file_with_path=csv_file_with_path, data=item, is_interactive=is_interactive)


def get_checked_branches_csv_file_pattern(desired_git_hub_repository: str, another_branch_name: str,
                                          current_branch_name: str, forked_git_hub_repository: str):
    return desired_git_hub_repository + ":" + current_branch_name \
        + '<-' + forked_git_hub_repository + ':' + another_branch_name


# def get_timeout_occurred_branches_csv_file_pattern(desired_git_hub_repository: str, another_branch_name: str,
#                                                    current_branch_name: str, forked_git_hub_repository: str):
#     return get_checked_branches_csv_file_pattern(desired_git_hub_repository=desired_git_hub_repository,
#                                                  another_branch_name=another_branch_name,
#                                                  current_branch_name=current_branch_name,
#                                                  forked_git_hub_repository=forked_git_hub_repository)


def get_appropriate_timeout_for_different_commits(different_commits_count: int) -> int:
    timeout = int((different_commits_count / 5) * 30)
    return choose_appropriate_timeout(calculated_timeout=timeout)


def get_appropriate_timeout_for_different_files(different_files_count: int) -> int:
    timeout = int(((different_files_count / 5) / 5) * 30)
    return choose_appropriate_timeout(calculated_timeout=timeout)


def choose_appropriate_timeout(calculated_timeout: int) -> int:
    if calculated_timeout < constants.default_input_timeout:
        return constants.default_input_timeout
    else:
        return calculated_timeout


def abnormal_condition_handler(trace_back: Traceback) -> None:
    print('From: ' + trace_back.filename + ' On Line Number ' +
          str(trace_back.lineno) + ' : Abnormal condition...')
    input('Press Enter to continue...')


def sort_paginated_list(paginated_list: PaginatedList) -> PaginatedList:
    list_length = paginated_list.totalCount
    for i in range(0, list_length):
        for j in range(1, list_length):
            current_element: Repository = paginated_list[i]
            next_element: Repository = paginated_list[j]
            if current_element.full_name > next_element.full_name:
                print('Exchange ' + current_element.full_name +
                      ' with ' + next_element.full_name)

    return paginated_list


def rate_limit_reached_handler():
    print('Rate Limit Reached, Please wait...')
    time.sleep(constants.rate_limit_sleep_time)


def no_internet_handler():
    time.sleep(constants.no_internet_sleep_time)


# def recursive_iteration_of_check_branches(function: Callable[..., tuple[bool, bool]]):

#     is_rate_limit_reached: bool
#     is_web_driver_exception_occurred: bool

#     while True:

#         is_rate_limit_reached, is_web_driver_exception_occurred = function()

#         if is_rate_limit_reached:

#             rate_limit_reached_handler()

#         elif is_web_driver_exception_occurred:

#             no_internet_handler()

#         else:
#             break

#         print('Retrying '+function.func_name+'...')


def web_driver_exception_handler(web_driver_exception: WebDriverException):
    print('WebDriverException : ' + web_driver_exception.msg + ', Please wait...')


def unexpected_response_handler():
    time.sleep(constants.unexpected_response_sleep_time)


def sign_in_github(driver: WebDriver):
    github_username: str | None = dotenv_values(verbose=True, interpolate=False).get('GITHUB_USERNAME')
    github_password: str | None = dotenv_values(verbose=True, interpolate=False).get('GITHUB_PASSWORD')

    if not (github_username and github_password):
        print("GitHub credentials not found in .env file. Exiting.")
        driver.quit()
        sys.exit(1)

    try:
        driver.get(constants.git_hub_url + '/' + constants.GithubUrls.GithubUrlEnums.LOGIN_PAGE.value)
        username_text_box: WebElement = driver.find_element(by=By.ID, value="login_field")
        username_text_box.send_keys(github_username)
        password_text_box: WebElement = driver.find_element(by=By.ID, value="password")
        password_text_box.send_keys(github_password)
        sign_in_button: WebElement = driver.find_element(by=By.NAME, value="commit")
        sign_in_button.click()

        # TODO : Check if the user is logged in or not
        # TODO : If the user is not logged in, check for verification code from mail enter page
        # TODO : If verification code from mail enter page, Open Mail login page
        # TODO : Enter the credentials for mail login page
        # TODO : If the user logged in, Open the verification mail, get the verification code
        # TODO : Enter the verification code in the verification code text box, then submit the code
        # TODO : Check if the user is logged in or not
        # TODO : If the user is not logged in, call abnormal condition handler
        # Note : Currently, Enter the code in the verification code text box manually, then submit the code manually

        # try:
        #     driver.find_element(by=By.ID, value="otp")
        #     input('After Verification, Press Enter to continue...')
        # except NoSuchElementException:
        #     pass
        input('After Verification, Press Enter to continue...')

    except WebDriverException as web_Driver_exception:
        web_driver_exception_handler(web_driver_exception=web_Driver_exception)


def initial_actions() -> tuple[WebDriver, str]:
    driver: WebDriver
    desired_git_hub_repository: str

    driver, desired_git_hub_repository = initialize()
    sign_in_github(driver=driver)
    return driver, desired_git_hub_repository


def get_branches_using_web_driver(driver: WebDriver, desired_git_hub_repository: str) -> \
        tuple[bool, bool, list[str], str]:
    """Returns a list of branches using a web browser

    Args:
        driver (webdriver): driver for web browser
        desired_git_hub_repository (str): desired GitHub repository name

    Returns:
        tuple[bool, bool, list[str], str]: [is web driver exception occurred,
                                            is rate limit reached,
                                            repository's branch names list,
                                            web driver exception message]
    """
    try:
        driver.get(constants.get_full_git_hub_url(desired_git_hub_repository=desired_git_hub_repository,
                                                  git_hub_website_url=str(
                                                      constants.GithubUrls.GithubUrlEnums.ALL_BRANCHES.value)))

        repository_branch_names: list[str] = []
        while True:

            branches = driver.find_elements(by=By.CLASS_NAME, value='branch-name')
            if branches:

                for branch in branches:
                    repository_branch_names.append(branch.text)

                try:
                    pagination = driver.find_element(by=By.CLASS_NAME, value='pagination')
                    active_pagination_buttons = pagination.find_elements(by=By.TAG_NAME, value='a')
                    next_page_button = check_branches.get_next_button_from_pagination_buttons(
                        active_pagination_buttons=active_pagination_buttons)

                    if next_page_button is None:
                        break

                    driver.get(next_page_button.get_attribute('href'))

                except NoSuchElementException:
                    break

            else:
                whoa_there_list: list[WebElement] = driver.find_elements(by=By.TAG_NAME, value='h1')
                if whoa_there_list:
                    h1_text: str = whoa_there_list[0].text
                    if h1_text == 'Whoa there!':
                        return True, False, [], ''

                    elif h1_text == 'Your connection was interrupted':
                        return retry_get_branches_using_web_driver_with_refresh(
                            driver=driver, desired_git_hub_repository=desired_git_hub_repository)
                    else:
                        abnormal_condition_handler(trace_back=getframeinfo(currentframe()))
                else:
                    return retry_get_branches_using_web_driver_with_refresh(
                        driver=driver, desired_git_hub_repository=desired_git_hub_repository)

        return False, False, repository_branch_names, ''

    except WebDriverException as web_driver_exception:

        web_driver_exception_handler(web_driver_exception=web_driver_exception)
        return False, True, [], web_driver_exception.msg


# def get_branches_using_github_api(git_hub_repository: Repository) -> tuple[bool, bool, list[str], str]:
#     """
#     Returns a list of branches using the PyGithub package
#
#     Args:
#         git_hub_repository (Repository): desired GitHub repository
#
#     Returns:
#         tuple[bool, bool, list[str], str]: [is rate limit reached,
#                                             is exception occurred,
#                                             repository's branch names list,
#                                             exception message]
#     """
#     try:
#         repository_branch_names: list[str] = [branch.name for branch in git_hub_repository.get_branches()]
#         return False, False, repository_branch_names, ''
#
#     except RateLimitExceededException:
#         return True, False, [], 'GitHub API rate limit exceeded, Please try again later.'
#
#     except GithubException as github_exception:
#         return False, True, [], github_exception.data.get('message', '')
#
#     except Exception as other_exception:
#         return False, True, [], str(other_exception)


def get_branches_using_web_driver_with_retries(driver: WebDriver, desired_git_hub_repository: str) -> list[str]:
    is_rate_limit_reached: bool
    is_web_driver_exception_occurred: bool
    repository_branch_names: list[str]
    web_driver_exception_message: str

    while True:

        is_rate_limit_reached, is_web_driver_exception_occurred, repository_branch_names, web_driver_exception_message = get_branches_using_web_driver(
            driver=driver, desired_git_hub_repository=desired_git_hub_repository)

        if is_rate_limit_reached:

            rate_limit_reached_handler()

        elif is_web_driver_exception_occurred:

            if constants.exception_message_identifier_for_unexpected_response in web_driver_exception_message:

                unexpected_response_handler()

            else:

                no_internet_handler()

        else:

            break

        print('Retrying get_branches_using_web_driver...')

    return repository_branch_names


def read_file_to_string(file_path: str) -> str:
    """Returns the contents of the file as a string

    Args:
        file_path (str): path of the file

    Returns:
        str: contents of the file
    """
    with open(file_path, 'r') as file:
        return file.read()


def disappear_element(element: WebElement, driver: WebDriver) -> None:
    """Disappears the element

    Args:
        @param element: element to be disappeared
        @param driver: browser driver
    """
    driver.execute_script(read_file_to_string(
        file_path='disappear_element.js'), element)


def change_inner_html_of_element(element: WebElement, driver: WebDriver, new_inner_html: str) -> None:
    """Changes the inner HTML of the element

    Args:
        element (WebElement): element to be changed
        driver (webdriver): driver for web browser
        new_inner_html (str): new inner HTML of the element
    """
    driver.execute_script(read_file_to_string(
        file_path='change_inner_html.js').replace('\n', '').replace('Value', new_inner_html), element)


def is_elements_are_disappeared(elements: list[WebElement]) -> bool:
    for element in elements:
        if element.is_displayed():
            return False
    return True


def input_timeout_handler(branch_info: str) -> None:
    print(branch_info + ', Interested - yes or no (default) : Timeout')


def interested_commits_handler(interested_branches_csv_file_with_path: str, desired_git_hub_repository: str,
                               branch_name: str, interested_commits_csv_file_with_path: str,
                               commit_ids_with_elements: dict[str, WebElement],
                               already_checked_commits: list[str]) -> None:
    append_single_column_row_to_csv(
        csv_file_with_path=interested_branches_csv_file_with_path,
        data=constants.get_git_hub_repository_url(
            desired_git_hub_repository=desired_git_hub_repository) + " " + branch_name)
    append_list_to_csv(
        csv_file_with_path=interested_commits_csv_file_with_path,
        list_to_append=[commit_id for commit_id in commit_ids_with_elements.keys() if
                        commit_id not in already_checked_commits])


def not_interested_commits_handler(skipped_commits_csv_file_with_path: str,
                                   commit_ids_with_elements: dict[str, WebElement],
                                   already_checked_commits: list[str]) -> None:
    if commit_ids_with_elements:
        append_list_to_csv(csv_file_with_path=skipped_commits_csv_file_with_path,
                           list_to_append=[commit_id for commit_id in commit_ids_with_elements.keys() if
                                           commit_id not in already_checked_commits])


def get_file_names(log_file_suffix: str) -> tuple[str, str, str, str, str, str]:
    return constants.checked_branches_csv + log_file_suffix, constants.interested_branches_csv + log_file_suffix, constants.input_timeout_occurred_branches_csv + log_file_suffix, constants.page_loading_timeout_occurred_branches_csv + log_file_suffix, constants.skipped_commits_csv + log_file_suffix, constants.interested_commits_csv + log_file_suffix


def is_repository_exists(driver: WebDriver, github_repository_url: str) -> bool:
    while True:
        try:

            driver.get(github_repository_url)
            break

        except WebDriverException as web_driver_exception:

            web_driver_exception_handler(
                web_driver_exception=web_driver_exception)

    try:

        driver.find_element(by=By.ID, value="not-found-search")
        return False

    except NoSuchElementException:

        return True


def print_inner_loop_iteration_separator() -> None:
    print('-----------------------------------------------------')


def print_outer_loop_iteration_separator() -> None:
    print('+++++++++++++++++++++++++++++++++++++++++++++++++++++')


def refresh_on_network_changed_error(driver: WebDriver) -> None:
    try:

        error_code_element_text: str = driver.find_element(by=By.CLASS_NAME, value='error-code').text
        if error_code_element_text == 'ERR_NETWORK_CHANGED':
            print('Error : ' + driver.find_element(by=By.CSS_SELECTOR,
                                                   value='[jsselect="heading"]').text + ', ' + driver.find_element(
                by=By.CSS_SELECTOR, value='[jsselect="summary"]').text + ', ' + error_code_element_text)

    except NoSuchElementException:

        pass

    print('Refreshing current page...')
    driver.refresh()


def retry_get_branches_using_web_driver_with_refresh(driver: WebDriver, desired_git_hub_repository: str) -> tuple[
    bool, bool, list[str], str]:
    refresh_on_network_changed_error(driver=driver)
    return get_branches_using_web_driver(driver=driver, desired_git_hub_repository=desired_git_hub_repository)
