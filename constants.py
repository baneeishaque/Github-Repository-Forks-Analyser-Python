from enum import Enum

git_hub_url: str = "https://github.com"


class GithubUrls:
    """
    Class to hold all the URLs for the GitHub website.
    """

    class GithubUrlEnums(Enum):
        ALL_FORKS = "network/members"
        ALL_BRANCHES = "branches/all"
        LOGIN_PAGE = "login"

    @staticmethod
    def compare(items_to_compare_as_url: str) -> str:
        return "compare/" + items_to_compare_as_url


def get_git_hub_repository_url(desired_git_hub_repository: str) -> str:
    """
    Returns the full URL for GitHub repository.
    """
    return git_hub_url + "/" + desired_git_hub_repository


def get_full_git_hub_url(desired_git_hub_repository: str, git_hub_website_url: str) -> str:
    return git_hub_url + "/" + desired_git_hub_repository + "/" + git_hub_website_url


def get_git_hub_all_branches_url(desired_git_hub_repository: str) -> str:
    """
    Returns the full URL for all branches of a GitHub repository.
    """
    return get_full_git_hub_url(desired_git_hub_repository=desired_git_hub_repository,
                                git_hub_website_url=str(GithubUrls.GithubUrlEnums.ALL_BRANCHES))


def get_git_hub_all_forks_url(desired_git_hub_repository: str) -> str:
    """
    Returns the full URL for all forks of a GitHub repository.
    """
    return get_full_git_hub_url(desired_git_hub_repository=desired_git_hub_repository,
                                git_hub_website_url=str(GithubUrls.GithubUrlEnums.ALL_FORKS))


def get_git_hub_branch_compare_url(desired_git_hub_repository: str, repository_owner: str,
                                   branch_name: str, default_branch: str = "master") -> str:
    """
    Returns the full URL of branch compare of a GitHub repository.
    Note: The default branch is used for comparison.
    """
    return get_full_git_hub_url(desired_git_hub_repository=desired_git_hub_repository,
                                git_hub_website_url=GithubUrls.compare(
                                    items_to_compare_as_url=default_branch + "..." + repository_owner + ":" + branch_name))


skipped_commits_csv: str = 'skippedCommits'
interested_commits_csv: str = 'interestedCommits'
checked_branches_csv: str = 'checkedBranches'
interested_branches_csv: str = 'interestedBranches'
input_timeout_occurred_branches_csv: str = 'inputTimeoutOccurredBranches'
page_loading_timeout_occurred_branches_csv: str = 'pageLoadingTimeoutOccurredBranches'

rate_limit_sleep_time: int = 30
no_internet_sleep_time: int = 120
unexpected_response_sleep_time: int = 1

default_input_timeout: int = 30

exception_message_identifier_for_unexpected_response: str = "unknown error: unexpected command response"

load_more_wait_time: int = 2

n_a: str = ' : N/A'

any_thing_to_compare_message: str = 'There isn’t anything to compare.'
