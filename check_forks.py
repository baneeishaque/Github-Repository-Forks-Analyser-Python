from inspect import currentframe, getframeinfo
from datetime import datetime, timedelta

import github.Auth
from dotenv import dotenv_values
from github import Github, Repository, PaginatedList
from playsound import playsound
from selenium.common.exceptions import NoSuchElementException, TimeoutException, WebDriverException
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

import check_branches
import common_functions
import constants


def get_git_hub_token() -> str:
    # TODO: Move to common utils - get .env vlue (use dotenv_path, verbose & interpolate)
    git_hub_token: str | None = dotenv_values(verbose=True, interpolate=False).get('GITHUB_TOKEN')

    assert git_hub_token is not None, 'GITHUB_TOKEN not found in .env file'
    return git_hub_token


def get_git_hub_repository(git_hub_repository: str) -> Repository:
    """
    Retrieves a GitHub repository using its full name (organization/repository).

    Args:
        git_hub_repository (str): The full name of the GitHub repository.

    Returns:
        Repository: The corresponding GitHub repository object.
    """
    return Github(auth=github.Auth.Token(token=get_git_hub_token())).get_repo(full_name_or_id=git_hub_repository)


def get_blank_git_hub_repository() -> Repository:
    return get_git_hub_repository(git_hub_repository='Sample_User_or_Organization/Sample_Repository')


def get_git_hub_repository_forks(desired_git_hub_repository: str) -> PaginatedList:
    git_hub_repository: Repository = get_git_hub_repository(
        git_hub_repository=desired_git_hub_repository)

    print("Total No. of Forks from Repo : " +
          str(git_hub_repository.forks_count))

    git_hub_repository_forks: PaginatedList = git_hub_repository.get_forks(
    )
    print("Total No. of Forks from Forks : " +
          str(git_hub_repository_forks.totalCount))

    # TODO : Sort list of forks - alphabetical order
    # git_hub_repository_forks = common_functions.sort_paginated_list(paginated_list=git_hub_repository_forks)

    return git_hub_repository_forks


def parse_fork_comparison_page(driver: WebDriver, skipped_commits_csv_file_with_path: str, repository_info: str,
                               interested_branches_csv_file_with_path: str, interested_commits_csv_file_with_path: str,
                               fork: Repository, branch_name: str, is_repository_info_printed: bool,
                               desired_git_hub_repository: str, different_commits_count: int,
                               different_files_count: int) -> tuple[str, bool, bool, bool]:
    is_input_timeout_occurred: bool = False
    is_page_loading_timeout_occurred: bool = False

    try:
        commit_ids_with_elements: dict[str, WebElement] = check_branches.get_commit_ids_with_elements(
            desired_git_hub_repository=desired_git_hub_repository, driver=driver)

        if common_functions.is_list_already_included_in_csv(csv_file_with_path=skipped_commits_csv_file_with_path,
                                                            list_to_check=list(commit_ids_with_elements.keys())):

            repository_info += common_functions.not_interested_commits_only

        else:

            if common_functions.is_list_already_included_in_csv(
                    csv_file_with_path=interested_commits_csv_file_with_path,
                    list_to_check=list(commit_ids_with_elements.keys())):

                repository_info += common_functions.interested_commits_only

            elif common_functions.is_list_already_included_among_two_csv_files(
                    csv1_file_with_path=skipped_commits_csv_file_with_path,
                    csv2_file_with_path=interested_commits_csv_file_with_path,
                    list_to_check=list(commit_ids_with_elements.keys())):

                repository_info += common_functions.not_interested_and_interested_commits_only

            else:

                already_skipped_commits: list[str] = common_functions.inner_join_list_with_csv(
                    csv_file_with_path=skipped_commits_csv_file_with_path,
                    list_to_check=list(commit_ids_with_elements.keys()))
                already_interested_commits: list[str] = common_functions.inner_join_list_with_csv(
                    csv_file_with_path=interested_commits_csv_file_with_path,
                    list_to_check=list(commit_ids_with_elements.keys()))
                already_checked_commits: list[str] = already_skipped_commits + \
                                                     already_interested_commits

                no_of_commits: int = len(commit_ids_with_elements)
                # print(repository_info + ', No. of Commits: ' + str(no_of_commits))
                no_of_already_checked_commits: int = len(
                    already_checked_commits)
                print(repository_info + ', No. of Already Checked Commits: ' +
                      str(no_of_already_checked_commits))

                no_of_different_commits: int = no_of_commits - no_of_already_checked_commits
                print(repository_info + ', No. of Different Commits: ' +
                      str(no_of_different_commits))

                # print(repository_info + ', Already checked commits: ' +
                #       str(already_checked_commits))

                if no_of_different_commits > 0:

                    for already_checked_commit in already_checked_commits:

                        already_checked_commit_element: WebElement = commit_ids_with_elements[
                            already_checked_commit]
                        already_checked_commit_element_card: WebElement = already_checked_commit_element.find_element(
                            by=By.XPATH, value='./../../..')
                        common_functions.disappear_element(
                            element=already_checked_commit_element_card, driver=driver)

                        already_checked_commit_element_parent: WebElement = already_checked_commit_element_card.find_element(
                            by=By.XPATH, value='./..')
                        already_checked_commit_element_parent_children: list[
                            WebElement] = already_checked_commit_element_parent.find_elements(
                            by=By.TAG_NAME, value='li')
                        if common_functions.is_elements_are_disappeared(
                                elements=already_checked_commit_element_parent_children):
                            common_functions.disappear_element(
                                element=already_checked_commit_element_parent.find_element(
                                    by=By.XPATH, value='./../..'), driver=driver)

                    footer_tag: WebElement = driver.find_element(
                        by=By.TAG_NAME, value='footer')
                    common_functions.change_inner_html_of_element(
                        element=footer_tag, driver=driver,
                        new_inner_html=common_functions.read_file_to_string(file_path='interested_or_not.html').replace(
                            '\n', '').replace("'", r"\'") + footer_tag.get_attribute('innerHTML').replace("\n", ""))

                    appropriate_timeout: int = constants.default_input_timeout
                    if different_commits_count > 0:

                        appropriate_timeout = common_functions.get_appropriate_timeout_for_different_commits(
                            different_commits_count=no_of_different_commits)

                    elif different_files_count > 0:

                        appropriate_timeout = common_functions.get_appropriate_timeout_for_different_files(
                            different_files_count=different_files_count)

                    else:

                        common_functions.abnormal_condition_handler(
                            trace_back=getframeinfo(currentframe()))

                    out_of_time = datetime.now() + timedelta(seconds=appropriate_timeout)
                    playsound(
                        sound='mixkit-urgent-simple-tone-loop-2976.wav', block=False)
                    print(repository_info + ', Interested [valid till ' +
                          out_of_time.strftime("%H:%M:%S") + '] - yes or no : ?')

                    while True:

                        interested_checkbox_is_checked: bool = driver.find_element(
                            by=By.ID, value='interestedCheckbox').is_selected()
                        # print(branch_info + ', Interested Checkbox State: ' +
                        #       str(interestedCheckboxIsChecked))

                        if interested_checkbox_is_checked:
                            print(repository_info +
                                  ', Interested - yes or no : Yes')
                            common_functions.interested_commits_handler(
                                interested_branches_csv_file_with_path=interested_branches_csv_file_with_path,
                                desired_git_hub_repository=fork.full_name, branch_name=branch_name,
                                interested_commits_csv_file_with_path=interested_commits_csv_file_with_path,
                                commit_ids_with_elements=commit_ids_with_elements,
                                already_checked_commits=already_checked_commits)

                            return repository_info, True, is_input_timeout_occurred, is_page_loading_timeout_occurred

                        not_interested_checkbox_is_checked: bool = driver.find_element(
                            by=By.ID, value='notInterestedCheckbox').is_selected()
                        # print(branch_info + ', Not Interested Checkbox State: ' +
                        #       str(nonInterestedCheckboxIsChecked))

                        if not_interested_checkbox_is_checked:
                            print(repository_info +
                                  ', Interested - yes or no : No')
                            common_functions.not_interested_commits_handler(
                                skipped_commits_csv_file_with_path=skipped_commits_csv_file_with_path,
                                commit_ids_with_elements=commit_ids_with_elements,
                                already_checked_commits=already_checked_commits)

                            return repository_info, True, is_input_timeout_occurred, is_page_loading_timeout_occurred

                        if datetime.now() > out_of_time:
                            common_functions.input_timeout_handler(
                                branch_info=repository_info)

                            return repository_info, True, True, is_page_loading_timeout_occurred

                else:

                    common_functions.abnormal_condition_handler(
                        trace_back=getframeinfo(currentframe()))

    except TimeoutException:

        repository_info += common_functions.page_loading_time_out_error
        is_page_loading_timeout_occurred = True

    return repository_info, is_repository_info_printed, is_input_timeout_occurred, is_page_loading_timeout_occurred


def check_fork(driver: WebDriver, fork: Repository, fork_index: int,
               desired_git_hub_repository: str, parent_branch_name: str, parent_branch_index: int,
               fork_branch_name: str,
               fork_branch_index: int) -> tuple[bool, bool, str]:
    # TODO : Consider grand forks
    # TODO : Use Commit Content Based Comparison
    # TODO : Handle entirely different commit histories

    log_file_suffix: str = '-' + \
                           desired_git_hub_repository.replace('/', '_') + '.csv'
    checked_branches_csv: str
    interested_branches_csv: str
    input_timeout_occurred_branches_csv: str
    page_loading_timeout_occurred_branches_csv: str
    skipped_commits_csv: str
    interested_commits_csv: str

    checked_branches_csv, interested_branches_csv, input_timeout_occurred_branches_csv, page_loading_timeout_occurred_branches_csv, skipped_commits_csv, interested_commits_csv = common_functions.get_file_names(
        log_file_suffix=log_file_suffix)

    repository_info: str = desired_git_hub_repository + ' : ' + parent_branch_name + \
                           '(' + str(parent_branch_index) + ') <- ' + fork.full_name + '[' + str(fork_index) + ']' + \
                           ' : ' + fork_branch_name + \
                           '(' + str(fork_branch_index) + ')'

    is_repository_info_printed = False
    is_input_timeout_occurred: bool = False
    is_page_loading_timeout_occurred: bool = False

    checked_branches_csv_file_pattern: str = common_functions.get_checked_branches_csv_file_pattern(
        desired_git_hub_repository=desired_git_hub_repository, another_branch_name=fork_branch_name,
        current_branch_name=parent_branch_name, forked_git_hub_repository=fork.full_name)

    if common_functions.is_item_included_in_csv(csv_file_with_path=input_timeout_occurred_branches_csv,
                                                data=checked_branches_csv_file_pattern):

        repository_info += common_functions.already_timeout

    else:

        if common_functions.is_item_included_in_csv(csv_file_with_path=checked_branches_csv,
                                                    data=checked_branches_csv_file_pattern):

            repository_info += common_functions.already_checked_it

        else:

            try:

                driver.get(fork.html_url)
                try:

                    driver.find_element(by=By.CLASS_NAME,
                                        value='flash flash-warn flash-full border-top-0 text-center text-bold py-2')
                    repository_info += "(Archived)"

                except NoSuchElementException:

                    pass

                driver.get(
                    "https://github.com/" + desired_git_hub_repository + "/compare/" +
                    parent_branch_name + "..." + fork.owner.login + ":" + fork_branch_name)

                fork_comparison_summary: list = driver.find_elements(
                    by=By.CLASS_NAME, value="text-emphasized")

                if fork_comparison_summary:

                    repository_info, is_repository_info_printed, is_input_timeout_occurred, is_page_loading_timeout_occurred = process_diff_commits(
                        diff_commits_or_files_count=int(fork_comparison_summary[0].text.replace(',', '')),
                        driver=driver, skipped_commits_csv=skipped_commits_csv, repository_info=repository_info,
                        interested_branches_csv=interested_branches_csv, interested_commits_csv=interested_commits_csv,
                        fork=fork, is_repository_info_printed=is_repository_info_printed,
                        fork_branch_name=fork_branch_name, desired_git_hub_repository=desired_git_hub_repository)

                else:

                    try:

                        repository_info, is_repository_info_printed, is_input_timeout_occurred, is_page_loading_timeout_occurred = process_diff_commits(
                            diff_commits_or_files_count=int(
                                driver.find_element(by=By.CSS_SELECTOR, value='[href="#commits_bucket"]').find_element(
                                    by=By.TAG_NAME, value='span').text.replace(',', '')), driver=driver,
                            skipped_commits_csv=skipped_commits_csv, repository_info=repository_info,
                            interested_branches_csv=interested_branches_csv,
                            interested_commits_csv=interested_commits_csv, fork=fork,
                            is_repository_info_printed=is_repository_info_printed, fork_branch_name=fork_branch_name,
                            desired_git_hub_repository=desired_git_hub_repository)

                    except NoSuchElementException:

                        try:

                            repository_info, is_repository_info_printed, is_input_timeout_occurred, is_page_loading_timeout_occurred = process_diff_commits(
                                diff_commits_or_files_count=int(driver.find_element(by=By.CSS_SELECTOR,
                                                                                    value='[href="#files_bucket"]').find_element(
                                    by=By.TAG_NAME, value='span').text.replace(',', '').replace('+', '').replace('∞',
                                                                                                                 str(constants.default_input_timeout * 30))),
                                driver=driver, skipped_commits_csv=skipped_commits_csv, repository_info=repository_info,
                                interested_branches_csv=interested_branches_csv,
                                interested_commits_csv=interested_commits_csv, fork=fork,
                                is_repository_info_printed=is_repository_info_printed,
                                fork_branch_name=fork_branch_name,
                                desired_git_hub_repository=desired_git_hub_repository, is_files=True)

                        except NoSuchElementException:

                            any_thing_to_compare_list: list[WebElement] = driver.find_elements(
                                by=By.CLASS_NAME, value='mb-1')

                            if any_thing_to_compare_list:

                                title_index: int = 2
                                if len(any_thing_to_compare_list) <= 2:
                                    title_index = 0

                                if (any_thing_to_compare_list[
                                        title_index].text == constants.any_thing_to_compare_message) or (
                                        any_thing_to_compare_list[0].tag_name == 'div' and any_thing_to_compare_list[
                                    1].tag_name == 'h3' and any_thing_to_compare_list[
                                            1].text == constants.any_thing_to_compare_message):

                                    repository_info += common_functions.no_changes_pattern

                                else:

                                    common_functions.abnormal_condition_handler(
                                        trace_back=getframeinfo(currentframe()))

                            else:

                                whoa_there_list: list[WebElement] = driver.find_elements(
                                    by=By.TAG_NAME, value='h1')

                                if whoa_there_list:

                                    h1_text: str = whoa_there_list[0].text
                                    if h1_text == 'Whoa there!':

                                        return True, False, ''

                                    elif h1_text == 'Your connection was interrupted':

                                        return retry_check_fork_with_refresh(driver=driver, fork=fork,
                                                                             fork_index=fork_index,
                                                                             desired_git_hub_repository=desired_git_hub_repository,
                                                                             parent_branch_name=parent_branch_name,
                                                                             parent_branch_index=parent_branch_index,
                                                                             fork_branch_name=fork_branch_name,
                                                                             fork_branch_index=fork_branch_index)

                                    else:

                                        common_functions.abnormal_condition_handler(
                                            trace_back=getframeinfo(currentframe()))

                                else:

                                    return retry_check_fork_with_refresh(driver=driver, fork=fork,
                                                                         fork_index=fork_index,
                                                                         desired_git_hub_repository=desired_git_hub_repository,
                                                                         parent_branch_name=parent_branch_name,
                                                                         parent_branch_index=parent_branch_index,
                                                                         fork_branch_name=fork_branch_name,
                                                                         fork_branch_index=fork_branch_index)

                if is_input_timeout_occurred:

                    common_functions.append_single_column_row_to_csv(
                        csv_file_with_path=input_timeout_occurred_branches_csv,
                        data=checked_branches_csv_file_pattern)

                elif is_page_loading_timeout_occurred:

                    repository_info += ', Url - ' + driver.current_url
                    common_functions.append_single_column_row_to_csv(
                        csv_file_with_path=page_loading_timeout_occurred_branches_csv,
                        data=checked_branches_csv_file_pattern)

                else:

                    common_functions.append_single_column_row_to_csv(
                        csv_file_with_path=checked_branches_csv, data=checked_branches_csv_file_pattern)

            except WebDriverException as web_driver_exception:

                common_functions.web_driver_exception_handler(
                    web_driver_exception=web_driver_exception)
                return False, True, web_driver_exception.msg

    if not is_repository_info_printed:
        print(repository_info)

    return False, False, ''


def compare_own_branches_using_web_driver(driver: WebDriver,
                                          git_hub_repository: str,
                                          is_parent: bool = False,
                                          parent_repository_name: str = '',
                                          fork_index: int = 0) -> None:
    print('Comparison of ' +
          ((parent_repository_name + ' : ' + git_hub_repository + '[' + str(
              fork_index) + ']') if is_parent else git_hub_repository)
          + ' branches')

    # common_functions.recursive_iteration_of_check_branches(check_branches.check_branches_using_web_driver(
    #     driver=driver, desired_git_hub_repository=desired_git_hub_repository, is_parent=is_parent, parent_repository_name=parent_repository_name, fork_index=fork_index))

    is_rate_limit_reached: bool
    is_web_driver_exception_occurred: bool
    web_driver_exception_message: str

    while True:

        is_rate_limit_reached, is_web_driver_exception_occurred, web_driver_exception_message = check_branches.check_branches_using_web_driver(
            driver=driver, desired_git_hub_repository=git_hub_repository, is_parent=is_parent,
            parent_repository_name=parent_repository_name, fork_index=fork_index)

        if is_rate_limit_reached:

            common_functions.rate_limit_reached_handler()

        elif is_web_driver_exception_occurred:

            if constants.exception_message_identifier_for_unexpected_response in web_driver_exception_message:

                common_functions.unexpected_response_handler()

            else:

                common_functions.no_internet_handler()

        else:
            break

        print('Retrying check_branches_using_web_driver...')

    common_functions.print_outer_loop_iteration_separator()


def main() -> None:
    git_hub_repository: str = common_functions.get_desired_git_hub_repository()
    try:
        repository_branch_names: list[str] = [branch.name for branch in (
            get_git_hub_repository(git_hub_repository=git_hub_repository)).get_branches()]

        if len(repository_branch_names) > 1:
            compare_own_branches_using_github_api(git_hub_repository=git_hub_repository,
                                                  repository_branch_names=repository_branch_names)

        # # TODO : get forks from html page source
        # git_hub_repository_forks: PaginatedList = get_git_hub_repository_forks(
        #     desired_git_hub_repository=git_hub_repository)
        #
        # fork_index: int = 1
        # fork: Repository
        # for fork in git_hub_repository_forks:
        #
        #     if common_functions.is_repository_exists(driver=driver, github_repository_url=fork.html_url):
        #
        #         forked_git_hub_repository: str = fork.full_name
        #
        #         forked_git_hub_repository_branches: list[str] = \
        #             common_functions.get_branches_using_web_driver_with_retries(driver=driver,
        #                                                                         desired_git_hub_repository=forked_git_hub_repository)
        #
        #         #     if re.search('Rate Limit', driver.find_element(by=By.TAG_NAME, value='title').text):
        #         #
        #         #         print('Rate Limit Reached, Please try later...')
        #         #         sys.exit(2)
        #         #
        #         #     else:
        #         #
        #         #         print(desired_git_hub_repository + ': ' +
        #         #               fork.full_name + '[' + str(fork_index) + ']' + n_a)
        #
        #         # TODO : Option to skip own branch comparisons
        #         if len(forked_git_hub_repository_branches) > 1:
        #             compare_own_branches_using_web_driver(driver=driver,
        #                                                   git_hub_repository=forked_git_hub_repository,
        #                                                   is_parent=True,
        #                                                   parent_repository_name=git_hub_repository,
        #                                                   fork_index=fork_index)
        #
        #         parent_branch_index: int = 1
        #         desired_git_hub_repository_branch: str
        #         for desired_git_hub_repository_branch in repository_branches:
        #
        #             fork_branch_index: int = 1
        #             forked_git_hub_repository_branch: str
        #             for forked_git_hub_repository_branch in forked_git_hub_repository_branches:
        #
        #                 is_rate_limit_reached: bool
        #                 is_web_driver_exception_occurred: bool
        #                 web_driver_exception_message: str
        #
        #                 while True:
        #
        #                     is_rate_limit_reached, is_web_driver_exception_occurred, web_driver_exception_message = check_fork(
        #                         driver=driver, fork=fork, fork_index=fork_index,
        #                         desired_git_hub_repository=git_hub_repository,
        #                         parent_branch_name=desired_git_hub_repository_branch,
        #                         parent_branch_index=parent_branch_index,
        #                         fork_branch_name=forked_git_hub_repository_branch,
        #                         fork_branch_index=fork_branch_index)
        #
        #                     if is_rate_limit_reached:
        #
        #                         common_functions.rate_limit_reached_handler()
        #
        #                     elif is_web_driver_exception_occurred:
        #
        #                         if constants.exception_message_identifier_for_unexpected_response in web_driver_exception_message:
        #
        #                             common_functions.unexpected_response_handler()
        #
        #                         else:
        #
        #                             common_functions.no_internet_handler()
        #
        #                     else:
        #
        #                         break
        #
        #                     print('Retrying check_fork...')
        #
        #                 fork_branch_index += 1
        #
        #             common_functions.print_inner_loop_iteration_separator()
        #             parent_branch_index += 1
        #
        #     else:
        #
        #         print(git_hub_repository + ' : X(X) <- ' + fork.full_name +
        #               '[' + str(fork_index) + ']' + ' : X(X)' + constants.n_a)
        #
        #     common_functions.print_outer_loop_iteration_separator()
        #     fork_index += 1
        #
        # driver.quit()

    except AssertionError as e:
        print(f"Assertion error: {e}")


def compare_own_branches_using_github_api(git_hub_repository: str, repository_branch_names: list[str]) -> None:
    print('Comparison of ' + git_hub_repository + ' branches')
    check_branches.check_branches_using_github_api(git_hub_repository=git_hub_repository,
                                                   repository_branch_names=repository_branch_names)
    common_functions.print_outer_loop_iteration_separator()


def process_diff_commits(diff_commits_or_files_count: int, driver: WebDriver, skipped_commits_csv: str,
                         repository_info: str, interested_branches_csv: str, interested_commits_csv: str,
                         fork: Repository, is_repository_info_printed: bool, fork_branch_name: str,
                         desired_git_hub_repository: str, is_files: bool = False) -> tuple[str, bool, bool, bool]:
    is_input_timeout_occurred: bool = False
    is_page_loading_timeout_occurred: bool = False

    print(repository_info + ', No. of Diff ' +
          ('Files' if is_files else 'Commits') + ' : ' + str(diff_commits_or_files_count))

    if diff_commits_or_files_count > 0:

        repository_info, is_repository_info_printed, is_input_timeout_occurred, is_page_loading_timeout_occurred = parse_fork_comparison_page(
            driver=driver, skipped_commits_csv_file_with_path=skipped_commits_csv, repository_info=repository_info,
            interested_branches_csv_file_with_path=interested_branches_csv,
            interested_commits_csv_file_with_path=interested_commits_csv,
            fork=fork, branch_name=fork_branch_name, is_repository_info_printed=is_repository_info_printed,
            desired_git_hub_repository=desired_git_hub_repository,
            different_commits_count=(0 if is_files else diff_commits_or_files_count),
            different_files_count=(diff_commits_or_files_count if is_files else 0))

    else:

        repository_info += common_functions.no_changes_pattern

    return repository_info, is_repository_info_printed, is_input_timeout_occurred, is_page_loading_timeout_occurred


def retry_check_fork_with_refresh(driver: WebDriver, fork: Repository, fork_index: int,
                                  desired_git_hub_repository: str, parent_branch_name: str, parent_branch_index: int,
                                  fork_branch_name: str,
                                  fork_branch_index: int) -> tuple[bool, bool, str]:
    common_functions.refresh_on_network_changed_error(driver=driver)

    return check_fork(driver=driver, fork=fork, fork_index=fork_index,
                      desired_git_hub_repository=desired_git_hub_repository, parent_branch_name=parent_branch_name,
                      parent_branch_index=parent_branch_index, fork_branch_name=fork_branch_name,
                      fork_branch_index=fork_branch_index)


if __name__ == "__main__":
    main()
