import os
import csv
import common_functions

# file1 - file2
file1_name = 'checkedBranches.csv'
file2_name = 'interestedBranches.csv'
temporary_file_name = 'tmp.csv'
with open(file1_name) as file1, open(file2_name) as file2:
    reader_file1 = csv.reader(file1)
    reader_file1_rows = []
    for row in reader_file1:
        reader_file1_rows.append(row)

    reader_file2 = csv.reader(file2)
    reader_file2_rows = []
    for row in reader_file2:
        reader_file2_rows.append(row)

    for row in reader_file1_rows:
        if row not in reader_file2_rows:
            common_functions.append_single_column_row_to_csv(csv_file_with_path=temporary_file_name, data=row[0])

os.remove(file1_name)
os.rename(temporary_file_name, file1_name)
