import os
from datetime import datetime, timedelta
from inspect import currentframe, getframeinfo
import time

from playsound import playsound
from selenium.common.exceptions import NoSuchElementException, TimeoutException, WebDriverException
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains

import common_functions
import constants


def get_next_button_from_pagination_buttons(active_pagination_buttons: list) -> WebElement:
    for button in active_pagination_buttons:

        if button.text == 'Next':
            return button


def parse_branch_comparison_page(driver: WebDriver, skipped_commits_csv_file_with_path: str, branch_info: str,
                                 interested_branches_csv_file_with_path: str,
                                 interested_commits_csv_file_with_path: str, branch_name: str,
                                 is_branch_info_printed: bool, desired_git_hub_repository: str,
                                 different_commits_count: int, different_files_count: int) -> \
        tuple[str, bool, bool, bool]:
    is_input_timeout_occurred: bool = False
    is_page_loading_timeout_occurred: bool = False

    try:
        commit_ids_with_elements: dict[str, WebElement] = get_commit_ids_with_elements(
            desired_git_hub_repository=desired_git_hub_repository, driver=driver)

        if common_functions.is_list_already_included_in_csv(csv_file_with_path=skipped_commits_csv_file_with_path,
                                                            list_to_check=list(commit_ids_with_elements.keys())):

            branch_info += common_functions.not_interested_commits_only

        else:

            if common_functions.is_list_already_included_in_csv(
                    csv_file_with_path=interested_commits_csv_file_with_path,
                    list_to_check=list(commit_ids_with_elements.keys())):

                branch_info += common_functions.interested_commits_only

            elif common_functions.is_list_already_included_among_two_csv_files(
                    csv1_file_with_path=skipped_commits_csv_file_with_path,
                    csv2_file_with_path=interested_commits_csv_file_with_path,
                    list_to_check=list(commit_ids_with_elements.keys())):

                branch_info += common_functions.not_interested_and_interested_commits_only

            else:

                already_skipped_commits: list[str] = common_functions.inner_join_list_with_csv(
                    csv_file_with_path=skipped_commits_csv_file_with_path,
                    list_to_check=list(commit_ids_with_elements.keys()))
                already_interested_commits: list[str] = common_functions.inner_join_list_with_csv(
                    csv_file_with_path=interested_commits_csv_file_with_path,
                    list_to_check=list(commit_ids_with_elements.keys()))
                already_checked_commits: list[str] = already_skipped_commits + \
                                                     already_interested_commits

                no_of_commits: int = len(commit_ids_with_elements)
                # print(branch_info + ', No. of Commits: ' + str(no_of_commits))
                no_of_already_checked_commits: int = len(
                    already_checked_commits)
                print(branch_info + ', No. of Already Checked Commits: ' +
                      str(no_of_already_checked_commits))

                no_of_different_commits: int = no_of_commits - no_of_already_checked_commits
                print(branch_info + ', No. of Different Commits: ' +
                      str(no_of_different_commits))

                # print(branch_info + ', Already checked commits: ' +
                #       str(already_checked_commits))

                if no_of_different_commits > 0:

                    for already_checked_commit in already_checked_commits:

                        already_checked_commit_element: WebElement = commit_ids_with_elements[
                            already_checked_commit]
                        already_checked_commit_element_card: WebElement = already_checked_commit_element.find_element(
                            by=By.XPATH, value='./../../..')
                        common_functions.disappear_element(
                            element=already_checked_commit_element_card, driver=driver)

                        already_checked_commit_element_parent: WebElement = already_checked_commit_element_card.find_element(
                            by=By.XPATH, value='./..')
                        already_checked_commit_element_parent_children: list[
                            WebElement] = already_checked_commit_element_parent.find_elements(
                            by=By.TAG_NAME, value='li')
                        if common_functions.is_elements_are_disappeared(
                                elements=already_checked_commit_element_parent_children):
                            common_functions.disappear_element(
                                element=already_checked_commit_element_parent.find_element(
                                    by=By.XPATH, value='./../..'), driver=driver)

                    # print(branch_info + ', Already checked commits: ' +
                    #       str(already_checked_commits))

                    footer_tag: WebElement = driver.find_element(
                        by=By.TAG_NAME, value='footer')
                    common_functions.change_inner_html_of_element(
                        element=footer_tag, driver=driver,
                        new_inner_html=common_functions.read_file_to_string(file_path='interested_or_not.html').replace(
                            '\n', '').replace("'", r"\'") + footer_tag.get_attribute('innerHTML').replace("\n", ""))

                    appropriate_timeout: int = constants.default_input_timeout
                    if different_commits_count > 0:

                        appropriate_timeout = common_functions.get_appropriate_timeout_for_different_commits(
                            different_commits_count=no_of_different_commits)

                    elif different_files_count > 0:

                        appropriate_timeout = common_functions.get_appropriate_timeout_for_different_files(
                            different_files_count=different_files_count)

                    else:

                        common_functions.abnormal_condition_handler(
                            trace_back=getframeinfo(currentframe()))

                    out_of_time = datetime.now() + timedelta(seconds=appropriate_timeout)

                    done = False
                    while not done:
                        try:
                            playsound(
                                sound=os.path.dirname(__file__) + '\\mixkit-urgent-simple-tone-loop-2976.wav',
                                block=False)
                            done = True
                        except:
                            continue

                    print(branch_info + ', Interested [valid till ' +
                          out_of_time.strftime("%H:%M:%S") + '] - yes or no : ?')

                    while True:

                        interested_checkbox_is_checked: bool = driver.find_element(
                            by=By.ID, value='interestedCheckbox').is_selected()
                        # print(branch_info + ', Interested Checkbox State: ' +
                        #       str(interestedCheckboxIsChecked))

                        if interested_checkbox_is_checked:
                            print(branch_info +
                                  ', Interested - yes or no : Yes')
                            common_functions.interested_commits_handler(
                                interested_branches_csv_file_with_path=interested_branches_csv_file_with_path,
                                desired_git_hub_repository=desired_git_hub_repository, branch_name=branch_name,
                                interested_commits_csv_file_with_path=interested_commits_csv_file_with_path,
                                commit_ids_with_elements=commit_ids_with_elements,
                                already_checked_commits=already_checked_commits)

                            return branch_info, True, is_input_timeout_occurred, is_page_loading_timeout_occurred

                        not_interested_checkbox_is_checked: bool = driver.find_element(
                            by=By.ID, value='notInterestedCheckbox').is_selected()
                        # print(branch_info + ', Not Interested Checkbox State: ' +
                        #       str(nonInterestedCheckboxIsChecked))

                        if not_interested_checkbox_is_checked:
                            print(branch_info +
                                  ', Interested - yes or no : No')
                            common_functions.not_interested_commits_handler(
                                skipped_commits_csv_file_with_path=skipped_commits_csv_file_with_path,
                                commit_ids_with_elements=commit_ids_with_elements,
                                already_checked_commits=already_checked_commits)

                            return branch_info, True, is_input_timeout_occurred, is_page_loading_timeout_occurred

                        if datetime.now() > out_of_time:
                            common_functions.input_timeout_handler(
                                branch_info=branch_info)

                            return branch_info, True, True, is_page_loading_timeout_occurred

                else:

                    common_functions.abnormal_condition_handler(
                        trace_back=getframeinfo(currentframe()))

    except TimeoutException:

        branch_info += common_functions.page_loading_time_out_error
        is_page_loading_timeout_occurred = True

    return branch_info, is_branch_info_printed, is_input_timeout_occurred, is_page_loading_timeout_occurred


def get_commit_ids_with_elements(desired_git_hub_repository: str, driver: WebDriver) -> dict[str, WebElement]:
    WebDriverWait(driver=driver, timeout=45).until(
        expected_conditions.presence_of_element_located((By.CSS_SELECTOR, '[aria-label="View commit details"]')))

    commits_bucket: WebElement = driver.find_element(
        by=By.ID, value='commits_bucket')
    while True:

        try:

            load_more_button: WebElement = commits_bucket.find_element(
                by=By.CSS_SELECTOR, value='[data-disable-with="Loading more..."]')
            load_more_button.click()
            time.sleep(constants.load_more_wait_time)
            commits_bucket = driver.find_element(
                by=By.ID, value='commits_bucket')

        except NoSuchElementException:

            break

    commit_id_links: list[WebElement] = commits_bucket.find_elements(by=By.CSS_SELECTOR,
                                                                     value='[aria-label="View commit details"]')
    commit_ids_with_elements: dict[str, WebElement] = {}
    for commit_id_link in commit_id_links:
        commit_ids_with_elements[commit_id_link.get_property(name='href').replace(
            constants.get_git_hub_repository_url(desired_git_hub_repository=desired_git_hub_repository) + '/commit/',
            '')] = commit_id_link

    actions: ActionChains = ActionChains(driver)
    try:

        actions.move_to_element(driver.find_element(
            by=By.CSS_SELECTOR, value='[href="#commits_bucket"]')).perform()

    except NoSuchElementException:

        actions.move_to_element(driver.find_element(
            by=By.CLASS_NAME, value='text-emphasized')).perform()

    return commit_ids_with_elements


def check_branches_using_web_driver(driver: WebDriver,
                                    desired_git_hub_repository: str = None,
                                    is_parent: bool = False,
                                    parent_repository_name: str = '',
                                    fork_index: int = 0) -> tuple[bool, bool, str]:
    repository_branch_names: list[str] = \
        common_functions.get_branches_using_web_driver_with_retries(driver=driver,
                                                                    desired_git_hub_repository=desired_git_hub_repository)

    checking_repository_name: str = parent_repository_name if is_parent else desired_git_hub_repository
    log_file_suffix: str = '-' + \
                           checking_repository_name.replace('/', '_') + '.csv'

    checked_branches_csv: str
    interested_branches_csv: str
    input_timeout_occurred_branches_csv: str
    page_loading_timeout_occurred_branches_csv: str
    skipped_commits_csv: str
    interested_commits_csv: str

    checked_branches_csv, interested_branches_csv, input_timeout_occurred_branches_csv, page_loading_timeout_occurred_branches_csv, skipped_commits_csv, interested_commits_csv = \
        common_functions.get_file_names(log_file_suffix=log_file_suffix)

    branch_index: int = 1
    for current_branch_name in repository_branch_names:

        branch_info: str = (checking_repository_name + ' : ' + ((desired_git_hub_repository + '[' + str(
            fork_index) + ']') if is_parent else desired_git_hub_repository))

        repository_branch_names_with_out_current_branch_name: list[str] = [repository_branch_name for
                                                                           repository_branch_name in
                                                                           repository_branch_names if
                                                                           repository_branch_name != current_branch_name]

        repository_branch_names_with_out_current_branch_name.reverse()

        another_branch_index: int = 1
        for another_branch_name in repository_branch_names_with_out_current_branch_name:

            previous_branch_info: str = branch_info
            branch_info += ' : ' + current_branch_name + '(' + str(
                branch_index) + ') <- ' + another_branch_name + '(' + str(another_branch_index) + ')'

            is_branch_info_printed: bool = False
            is_input_timeout_occurred: bool = False
            is_page_loading_timeout_occurred: bool = False

            checked_branches_csv_file_pattern: str = common_functions.get_checked_branches_csv_file_pattern(
                desired_git_hub_repository=desired_git_hub_repository, another_branch_name=another_branch_name,
                current_branch_name=current_branch_name, forked_git_hub_repository=desired_git_hub_repository)
            # timeout_occurred_branches_csv_file_pattern: str =
            # common_functions.get_timeout_occurred_branches_csv_file_pattern(
            #     desired_git_hub_repository=desired_git_hub_repository, another_branch_name=another_branch_name,
            #     current_branch_name=current_branch_name, forked_git_hub_repository=desired_git_hub_repository)
            # timeout_occurred_branches_csv_file_pattern: str = checked_branches_csv_file_pattern

            if common_functions.is_item_included_in_csv(csv_file_with_path=input_timeout_occurred_branches_csv,
                                                        data=checked_branches_csv_file_pattern):

                branch_info += common_functions.already_timeout

            else:

                if common_functions.is_item_included_in_csv(csv_file_with_path=checked_branches_csv,
                                                            data=checked_branches_csv_file_pattern):

                    branch_info += common_functions.already_checked_it

                else:

                    try:

                        driver.get(constants.get_git_hub_branch_compare_url(
                            desired_git_hub_repository=desired_git_hub_repository,
                            repository_owner=desired_git_hub_repository.split(
                                '/')[0],
                            branch_name=another_branch_name, default_branch=current_branch_name))

                        branch_comparison_summary: list[WebElement] = driver.find_elements(by=By.CLASS_NAME,
                                                                                           value='text-emphasized')
                        if branch_comparison_summary:

                            branch_info, is_branch_info_printed, is_input_timeout_occurred, is_page_loading_timeout_occurred = process_diff_commits(
                                diff_commits_or_files_count=int(
                                    branch_comparison_summary[0].text.replace(',', '')), driver=driver,
                                skipped_commits_csv=skipped_commits_csv, branch_info=branch_info,
                                another_branch_name=another_branch_name,
                                interested_branches_csv=interested_branches_csv,
                                interested_commits_csv=interested_commits_csv,
                                is_branch_info_printed=is_branch_info_printed,
                                desired_git_hub_repository=desired_git_hub_repository)

                        else:

                            try:

                                branch_info, is_branch_info_printed, is_input_timeout_occurred, is_page_loading_timeout_occurred = process_diff_commits(
                                    diff_commits_or_files_count=int(driver.find_element(
                                        by=By.CSS_SELECTOR, value='[href="#commits_bucket"]').find_element(
                                        by=By.TAG_NAME, value='span').text.replace(',', '')), driver=driver,
                                    skipped_commits_csv=skipped_commits_csv, branch_info=branch_info,
                                    another_branch_name=another_branch_name,
                                    interested_branches_csv=interested_branches_csv,
                                    interested_commits_csv=interested_commits_csv,
                                    is_branch_info_printed=is_branch_info_printed,
                                    desired_git_hub_repository=desired_git_hub_repository)

                            except NoSuchElementException:

                                try:

                                    branch_info, is_branch_info_printed, is_input_timeout_occurred, is_page_loading_timeout_occurred = process_diff_commits(
                                        diff_commits_or_files_count=int(driver.find_element(by=By.CSS_SELECTOR,
                                                                                            value='[href="#files_bucket"]').find_element(
                                            by=By.TAG_NAME, value='span').text.replace(',', '').replace('+',
                                                                                                        '').replace('∞',
                                                                                                                    str(constants.default_input_timeout * 30))),
                                        driver=driver, skipped_commits_csv=skipped_commits_csv, branch_info=branch_info,
                                        another_branch_name=another_branch_name,
                                        interested_branches_csv=interested_branches_csv,
                                        interested_commits_csv=interested_commits_csv,
                                        is_branch_info_printed=is_branch_info_printed,
                                        desired_git_hub_repository=desired_git_hub_repository, is_files=True)

                                except NoSuchElementException:

                                    any_thing_to_compare_list: list[WebElement] = driver.find_elements(
                                        by=By.CLASS_NAME, value='mb-1')

                                    if any_thing_to_compare_list:

                                        title_index: int = 2
                                        if len(any_thing_to_compare_list) <= 2:
                                            title_index = 0

                                        if (any_thing_to_compare_list[
                                                title_index].text == constants.any_thing_to_compare_message) or (
                                                any_thing_to_compare_list[0].tag_name == 'div' and
                                                any_thing_to_compare_list[1].tag_name == 'h3' and
                                                any_thing_to_compare_list[
                                                    1].text == constants.any_thing_to_compare_message):

                                            branch_info += common_functions.no_changes_pattern

                                        else:

                                            common_functions.abnormal_condition_handler(
                                                trace_back=getframeinfo(currentframe()))

                                    else:

                                        whoa_there_list: list[WebElement] = driver.find_elements(
                                            by=By.TAG_NAME, value='h1')

                                        if whoa_there_list:

                                            h1_text: str = whoa_there_list[0].text
                                            if h1_text == 'Whoa there!':

                                                return True, False, ''

                                            elif h1_text == 'Your connection was interrupted':

                                                return retry_check_branches_using_web_driver_with_refresh(
                                                    driver=driver,
                                                    desired_git_hub_repository=desired_git_hub_repository,
                                                    is_parent=is_parent, parent_repository_name=parent_repository_name,
                                                    fork_index=fork_index)

                                            else:

                                                common_functions.abnormal_condition_handler(
                                                    trace_back=getframeinfo(currentframe()))

                                        else:

                                            return retry_check_branches_using_web_driver_with_refresh(
                                                driver=driver, desired_git_hub_repository=desired_git_hub_repository,
                                                is_parent=is_parent, parent_repository_name=parent_repository_name,
                                                fork_index=fork_index)

                        if is_input_timeout_occurred:

                            common_functions.append_single_column_row_to_csv(
                                csv_file_with_path=input_timeout_occurred_branches_csv,
                                data=checked_branches_csv_file_pattern)

                        elif is_page_loading_timeout_occurred:

                            branch_info += ', Url - ' + driver.current_url
                            common_functions.append_single_column_row_to_csv(
                                csv_file_with_path=page_loading_timeout_occurred_branches_csv,
                                data=checked_branches_csv_file_pattern)

                        else:

                            common_functions.append_single_column_row_to_csv(
                                csv_file_with_path=checked_branches_csv, data=checked_branches_csv_file_pattern)

                    except WebDriverException as web_driver_exception:

                        common_functions.web_driver_exception_handler(
                            web_driver_exception=web_driver_exception)
                        return False, True, web_driver_exception.msg

            if not is_branch_info_printed:
                print(branch_info)

            another_branch_index += 1
            branch_info = previous_branch_info

        common_functions.print_inner_loop_iteration_separator()
        branch_index += 1

    return False, False, ''


def process_diff_commits(diff_commits_or_files_count: int, driver: WebDriver, skipped_commits_csv: str,
                         branch_info: str, another_branch_name: str, interested_branches_csv: str,
                         interested_commits_csv: str, is_branch_info_printed: bool, desired_git_hub_repository: str,
                         is_files: bool = False) -> tuple[str, bool, bool, bool]:
    is_input_timeout_occurred: bool = False
    is_page_loading_timeout_occurred: bool = False

    print(branch_info + ', No. of Diff ' + ('Files' if is_files else 'Commits') +
          ' : ' + str(diff_commits_or_files_count))

    if diff_commits_or_files_count > 0:

        branch_info, is_branch_info_printed, is_input_timeout_occurred, is_page_loading_timeout_occurred = parse_branch_comparison_page(
            driver=driver, skipped_commits_csv_file_with_path=skipped_commits_csv,
            branch_info=branch_info, branch_name=another_branch_name,
            interested_branches_csv_file_with_path=interested_branches_csv,
            interested_commits_csv_file_with_path=interested_commits_csv,
            is_branch_info_printed=is_branch_info_printed,
            desired_git_hub_repository=desired_git_hub_repository,
            different_commits_count=(0 if is_files else diff_commits_or_files_count),
            different_files_count=(diff_commits_or_files_count if is_files else 0))

    else:

        branch_info += common_functions.no_changes_pattern

    return branch_info, is_branch_info_printed, is_input_timeout_occurred, is_page_loading_timeout_occurred


def main() -> None:
    # common_functions.recursive_iteration_of_check_branches(
    #     check_branches(constants.web_driver_path))

    driver: WebDriver
    desired_git_hub_repository: str

    driver, desired_git_hub_repository = common_functions.initial_actions()

    is_rate_limit_reached: bool
    is_web_driver_exception_occurred: bool
    web_driver_exception_message: str

    while True:

        is_rate_limit_reached, is_web_driver_exception_occurred, web_driver_exception_message = check_branches_using_web_driver(
            driver=driver, desired_git_hub_repository=desired_git_hub_repository)

        if is_rate_limit_reached:

            common_functions.rate_limit_reached_handler()

        elif is_web_driver_exception_occurred:

            if constants.exception_message_identifier_for_unexpected_response in web_driver_exception_message:

                common_functions.unexpected_response_handler()

            else:

                common_functions.no_internet_handler()

        else:

            break

        print('Retrying check_branches...')

    driver.quit()


def retry_check_branches_using_web_driver_with_refresh(driver: WebDriver, desired_git_hub_repository: str,
                                                       is_parent: bool, parent_repository_name: str, fork_index: int) -> \
        tuple[bool, bool, str]:
    common_functions.refresh_on_network_changed_error(
        driver=driver)

    return check_branches_using_web_driver(driver=driver, desired_git_hub_repository=desired_git_hub_repository,
                                           is_parent=is_parent, parent_repository_name=parent_repository_name,
                                           fork_index=fork_index)


def check_branches_using_github_api(git_hub_repository: str, repository_branch_names: list[str]):
    log_file_suffix: str = '-' + git_hub_repository.replace('/', '_') + '.csv'

    checked_branches_csv: str
    checked_branches_csv, _, _, _, _, _ = common_functions.get_file_names(log_file_suffix=log_file_suffix)

    branch_index: int = 1
    for current_branch_name in repository_branch_names:

        branch_info: str = (git_hub_repository + ' : ' + git_hub_repository)
        repository_branch_names_with_out_current_branch_name: list[str] = [repository_branch_name for
                                                                           repository_branch_name in
                                                                           repository_branch_names if
                                                                           repository_branch_name != current_branch_name]
        repository_branch_names_with_out_current_branch_name.reverse()

        another_branch_index: int = 1
        for another_branch_name in repository_branch_names_with_out_current_branch_name:

            previous_branch_info: str = branch_info
            branch_info += (' : ' + current_branch_name + '(' + str(branch_index) + ') <- '
                            + another_branch_name + '(' + str(another_branch_index) + ')')

            checked_branches_csv_file_pattern: str = common_functions.get_checked_branches_csv_file_pattern(
                desired_git_hub_repository=git_hub_repository, another_branch_name=another_branch_name,
                current_branch_name=current_branch_name, forked_git_hub_repository=git_hub_repository)

            if common_functions.is_item_included_in_csv(csv_file_with_path=checked_branches_csv,
                                                        data=checked_branches_csv_file_pattern):
                branch_info += common_functions.already_checked_it
            else:
                common_functions.append_single_column_row_to_csv(csv_file_with_path=checked_branches_csv,
                                                                 data=checked_branches_csv_file_pattern)

            print(branch_info)

            another_branch_index += 1
            branch_info = previous_branch_info

        common_functions.print_inner_loop_iteration_separator()
        branch_index += 1


if __name__ == "__main__":
    main()
