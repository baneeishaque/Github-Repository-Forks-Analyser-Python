from selenium import webdriver

import check_branches
import common_functions
import constants

driver: webdriver = common_functions.get_chrome_driver(
)

# driver.get('https://github.com/abc321def/whapa/compare/master...abc321def:patch-1')
# check_branches.parse_branch_comparison_page(
#     driver=driver,
#     skipped_branches_csv_file_with_path=constants.skipped_commits_csv + '-Test.csv',
#     branch_info='Branch 2 : patch-1', branch_name='patch-1',
#     interested_branches_csv_file_with_path=constants.interested_branches_csv + '-Test.csv',
#     interested_commits_csv_file_with_path=constants.interested_commits_csv + '-Test.csv',
#     is_branch_info_printed=False,
#     desired_git_hub_repository='abc321def/whapa')

# driver.get(
#     'https://github.com/firebase/flutterfire/compare/master...firebase:crashlytics-eap-release')
# check_branches.parse_branch_comparison_page(
#     driver=driver,
#     skipped_commits_csv_file_with_path=constants.skipped_commits_csv + '-Test.csv',
#     branch_info='Branch X : crashlytics-eap-release', branch_name='crashlytics-eap-release',
#     interested_branches_csv_file_with_path=constants.interested_branches_csv + '-Test.csv',
#     interested_commits_csv_file_with_path=constants.interested_commits_csv + '-Test.csv',
#     is_branch_info_printed=False,
#     desired_git_hub_repository='firebase/flutterfire', different_commits_count=0, different_files_count=0)

driver.get(
    'https://github.com/Arunbalaji001/getwidget/compare/imgbot...Arunbalaji001:master')
check_branches.parse_branch_comparison_page(
    driver=driver,
    skipped_commits_csv_file_with_path=constants.skipped_commits_csv + '-Test.csv',
    branch_info='Branch 4 : imgbot', branch_name='imgbot',
    interested_branches_csv_file_with_path=constants.interested_branches_csv + '-Test.csv',
    interested_commits_csv_file_with_path=constants.interested_commits_csv + '-Test.csv',
    is_branch_info_printed=False,
    desired_git_hub_repository='Arunbalaji001/getwidget', different_commits_count=44, different_files_count=0)
